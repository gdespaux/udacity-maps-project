var map;
var markers = [];
var globalMapPoints = [];
var largeInfoWindow;
var bounds;

var MapPoint = function (data, id) {
    this.title = data.title;
    this.location = {lat: data.location.lat, lng: data.location.lng};
    this.id = id;

    // This will make the marker visible on the map
    this.showMarker = function () {
        if (markers[this.id] !== void 0) {
            markers[this.id].setVisible(true);
        }
    };

    // This hides the marker without disassociating it from the map
    this.hideMarker = function () {
        markers[this.id].setVisible(false);
    };

    // Used by the list view when clicked to select this marker
    this.selectThisPoint = function () {
        marker = markers[this.id];
        getPhoto(marker);
    };
};

var AllLocations = function () {
    // Used by the filter input to filter list of locations
    this.filter = ko.observable('');

    this.staticData = [
        {title: 'Bruno\'s Tavern', location: {lat: 29.941320, lng: -90.128653}},
        {title: 'The Palms Bar and Grill', location: {lat: 29.941211, lng: -90.123438}},
        {title: 'Superior Grill', location: {lat: 29.927293, lng: -90.094383}},
        {title: 'Commander\'s Palace', location: {lat: 29.928734, lng: -90.084204}},
        {title: 'National WWII Museum', location: {lat: 29.942717, lng: -90.070557}},
        {title: 'New Orleans Saints', location: {lat: 29.950935, lng: -90.081157}}
    ];

    this.mapPoints = [
        new MapPoint(this.staticData[0], 0),
        new MapPoint(this.staticData[1], 1),
        new MapPoint(this.staticData[2], 2),
        new MapPoint(this.staticData[3], 3),
        new MapPoint(this.staticData[4], 4),
        new MapPoint(this.staticData[5], 5)
    ];

    // Bound to listview to display filtered results
    this.filteredPoints = ko.computed(function () {
        var filter = this.filter().toLowerCase();
        if (!filter) {
            for (var i = 0; i < this.mapPoints.length; i++) {
                this.mapPoints[i].showMarker();
            }
            return this.mapPoints;
        } else {
            var resultPoints = ko.utils.arrayFilter(this.mapPoints, function (mapPoint) {
                //return stringStartsWith(mapPoint.title.toLowerCase(), filter);
                return mapPoint.title.toLowerCase().indexOf(filter.toLowerCase()) >= 0;
            });
            for (var j = 0; j < this.mapPoints.length; j++) {
                this.mapPoints[j].hideMarker();
            }
            for (var k = 0; k < resultPoints.length; k++) {
                resultPoints[k].showMarker();
            }
            return resultPoints;
        }
    }, this);

    globalMapPoints = this.mapPoints;
};

ko.applyBindings(new AllLocations());

function mapLoadError() {
    // Let's give a simple message that we couldn't contact Google (It can happen!)
    alert('We weren\'t able to load the Google Maps API. Please try again later.');
}

function initMap() {
    // Creates a new map
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 29.9630365, lng: -90.0882388},
        zoom: 13
    });

    largeInfoWindow = new google.maps.InfoWindow();
    bounds = new google.maps.LatLngBounds();

    // Use the location array to create an array of markers
    for (var i = 0; i < globalMapPoints.length; i++) {
        /*jshint loopfunc: true */
        var position = globalMapPoints[i].location;
        var title = globalMapPoints[i].title;

        var marker = new google.maps.Marker({
            position: position,
            title: title,
            animation: google.maps.Animation.DROP,
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
            id: i
        });
        // Push the marker to our markers array
        markers.push(marker);
        // Extend boundaries for each marker added
        bounds.extend(marker.position);
        // Set listener for infoWindow
        marker.addListener('click', function () {
            getPhoto(this);
        });
    }

    showListings();
}

// Populate the infoWindow when a marker is clicked. Only one will be open at a time
function populateInfoWindow(marker, infoWindow, photo, user) {
    // Check if window is already open on this marker
    if (infoWindow.marker !== marker) {
        infoWindow.marker = marker;

        // Define content of the infoWindow
        if (photo === null || user === null) {
            infoWindowContent = '<h3>' + marker.title + '</h3>';
        } else {
            infoWindowContent = '<img src="' + photo + '"><h3>' +
                marker.title + '</h3><p>Photo by: <a href="' +
                user.links.html + '?utm_source=maps-project&utm_medium=referral&utm_campaign=api-credit">' +
                user.name + '</a> / <a href="https://unsplash.com?utm_source=maps-project&utm_medium=referral&' +
                'utm_campaign=api-credit">Unsplash</a></p>';
        }

        infoWindow.setContent(infoWindowContent);
        infoWindow.open(map, marker);

        // Animate marker to draw attention
        marker.setAnimation(google.maps.Animation.BOUNCE);
        setTimeout(function () {
            marker.setAnimation(null);
        }, 1400);

        // Clear marker if infoWindow is closed
        infoWindow.addListener('closeclick', function () {
            infoWindow.setMarker = null;
        });
    }
}

// Loop through and display all markers
function showListings() {
    var bounds = new google.maps.LatLngBounds();
    // Extend boundaries of map for each marker
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
        bounds.extend(markers[i].position);
    }
    map.fitBounds(bounds);
}

function getPhoto(marker) {
    // Uses the Unsplash API to get a photo using the marker's title as keywords
    // This doesn't guarantee a photo relating to the location, but gives a fun result
    var request = $.ajax({
        dataType: 'json',
        url: 'https://api.unsplash.com/search/photos',
        data: {
            client_id: '1ba6013bb3b264a9e3015c06824b1e0a2ba1cc639a8784fb840a62db936b9b34',
            query: marker.title
        },
        statusCode: {
            403: function () {
                console.log('Looks like the hourly quota was exceeded. No pictures will load for a while.');
                // API quota was exceeded. The user can't do anything about it, let's just load what we can
                populateInfoWindow(marker, largeInfoWindow);
            }
        }
    });

    request.done(function (result) {
        var photo = result.results[0].urls.thumb;
        // User info for required attribution to Unsplash artist
        var user = result.results[0].user;
        populateInfoWindow(marker, largeInfoWindow, photo, user);
    });

    request.fail(function (result) {
        // Request itself has failed. Let's let the user know, maybe they can retry
        alert('There was a problem retrieving a photo of the location. You can try again if you\'d like.');
        populateInfoWindow(marker, largeInfoWindow);
    });
}